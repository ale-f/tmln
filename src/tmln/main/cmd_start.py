import argparse

from ..filter import Filter, select_timelines_from
from ..timeline import ConsistencyError

from .utils import print_status
from .arg_types import fail, int_or, const_or, expression_or


def contribute_args(subparsers):
    start_args = subparsers.add_parser(
            "start-timeline",
            aliases=["start"],
            help="Begin, or resume, work on a timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Begin, or resume, work on a timeline.""")
    start_args.set_defaults(func=cmd_start)
    start_args.add_argument(
            "id",
            type=const_or("new", int_or(expression_or(fail(
                    "expected 'new', timeline ID or filter expression")))),
            metavar="expr|ID|new")


def cmd_start(tl, args):
    ts = args.timestamp
    stl = None
    if args.id == "new":
        stl = tl.get_new_specific_timeline()
    else:
        stl = select_timelines_from(tl, args.id)
        if len(stl) == 0:
            print("No such timeline.")
        elif len(stl) == 1:
            stl = stl.pop()
        else:
            print("Too many timelines.")
            print_status(*stl)
            return

    if stl:
        try:
            stl.start(ts)
            if not stl.has_parts():
                print("Work on timeline {0} begins at {1}.".format(stl.id, ts))
            else:
                print("Work on suspended timeline {0} resumes at {1}.".format(
                        stl.id, ts))
        except ConsistencyError as e:
            print(e)
    else:
        print("No such timeline.")
