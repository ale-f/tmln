import re
from json import loads, dumps, JSONDecodeError
from datetime import datetime, timedelta
from dateutil import tz


def delta_to_timespec(d):
    seconds = d.total_seconds()
    hours = int(seconds / 3600)
    seconds -= hours * 3600
    minutes = int(seconds / 60)
    seconds = int(seconds - (minutes * 60))
    return "{0}{1}{2}".format(
            (str(hours) + "h") if hours else "",
            (str(minutes) + "m") if minutes else "",
            (str(seconds) + "s") if seconds or not (hours or minutes) else "")


_ts_regex = re.compile(
        r"^(?P<sign>\+|-|)"
        r"((?P<hours>\d+)(h|t))?"
        r"((?P<minutes>\d+)m)?"
        r"((?P<seconds>\d+)s)?$", re.IGNORECASE)


def timespec_to_delta(ts):
    m = _ts_regex.match(ts)
    if m and (m.group("hours") or m.group("minutes") or m.group("seconds")):
        delta = timedelta(
                hours=int(m.group("hours") or 0),
                minutes=int(m.group("minutes") or 0),
                seconds=int(m.group("seconds") or 0))
        return delta if m.group("sign") != "-" else -delta
    else:
        return None


def str_to_json(s):
    try:
        return loads(s)
    except JSONDecodeError:
        # Treat anything we can't decode as JSON as a string value
        return s


def json_to_str(p):
    if isinstance(p, str):
        return p
    else:
        return dumps(p)


def datetime_now():
    return datetime.now(tz.gettz()).replace(microsecond=0)
