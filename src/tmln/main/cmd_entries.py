import argparse

from .utils import print_entries
from .arg_types import int_or


def contribute_args(subparsers):
    entries_args = subparsers.add_parser(
            "list-entries",
            aliases=["entries"],
            help="List all of the entries on a timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
List all of the entries on a timeline.""")
    entries_args.add_argument(
            "id",
            nargs="?",
            type=int_or("global"),
            metavar="ID|global")
    entries_args.set_defaults(func=cmd_entries)


def cmd_entries(tl, args):
    if args.id is None:
        stl = tl.active_timeline
    elif args.id == "global":
        stl = tl
    else:
        stl = tl.get_specific_timeline(args.id, create=False)

    if stl:
        print_entries(*stl.entries)
    else:
        print("No active timeline." if args.id is None
              else "No such timeline.")
