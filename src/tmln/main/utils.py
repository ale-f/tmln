from tabulate import tabulate

from ..utils import delta_to_timespec


def print_tabulate(rows, **kwargs):
    print(tabulate(rows, headers="firstrow", tablefmt="simple", **kwargs))


def print_status(*stls):
    lines = []
    all_properties = set()
    for stl in stls:
        all_properties.update(
                key for key in stl.properties.keys()
                if not key.startswith("."))
    all_properties = sorted(list(all_properties))
    lines.append(["id", *all_properties, "duration", "last_task"])
    for stl in stls:
        last_task = stl.last_task
        rendered_last_task = "(none)"
        if last_task:
            date, activity, duration = last_task
            rendered_last_task = "{0} ({1}, {2})".format(
                    activity, date, delta_to_timespec(duration))
        values = []
        for prop in all_properties:
            value = stl.get_property(prop)
            if value:
                if prop not in stl.properties:
                    representation = "<{0}>".format(value)
                else:
                    representation = value
            else:
                representation = "(none)"
            values.append(representation)
        lines.append([
                stl.id,
                *values,
                delta_to_timespec(stl.duration),
                rendered_last_task])
    print_tabulate(lines)


def print_entries(*entries):
    lines = [["timestamp", "id", "entry"]]
    for entry in entries:
        lines.append([
                entry.timestamp,
                entry.timeline if entry.timeline is not None else "(global)",
                entry.pretty_print()])
    print_tabulate(lines)
