from abc import ABC, abstractmethod
import json
from typing import Any
from datetime import datetime


DATE_FORMAT = "%Y-%m-%dT%H:%M:%S%z"


class Entry(ABC):
    def __init__(self, timeline: int, timestamp: datetime):
        self._timeline = timeline
        self._timestamp = timestamp

    @property
    def timeline(self):
        return self._timeline

    @property
    def timestamp(self):
        return self._timestamp

    def dump(self, fp):
        return json.dump(self.as_dict(), fp)
    save = dump

    def dumps(self):
        return json.dumps(self.as_dict())

    @abstractmethod
    def as_dict(self):
        return {
            "_nature": "fyi.ajf.tmln.entry",
            "timestamp": self._timestamp.strftime(DATE_FORMAT),
            "timeline": self._timeline
        }

    @classmethod
    @abstractmethod
    def from_json(cls, dct):
        pass

    def __str__(self):
        d = self.as_dict()
        kwargs = ", ".join(
                '{0}={1}'.format(k, repr(v)) for k, v in d.items()
                if k not in ("_nature", "timestamp", "timeline", "type"))
        return "{0}({1})".format(type(self).__name__, kwargs)

    @abstractmethod
    def pretty_print(self):
        pass


class StartEntry(Entry):
    def __init__(self, timeline: int, timestamp: datetime):
        super().__init__(timeline, timestamp)

    def as_dict(self):
        return dict(**super().as_dict(), **{
            "type": "start"
        })

    @classmethod
    def from_json(cls, dct):
        return cls(
                timeline=int(dct["timeline"]),
                timestamp=datetime.strptime(dct["timestamp"], DATE_FORMAT))

    def pretty_print(self):
        return "start"


class StopEntry(Entry):
    def __init__(self, timeline: int, timestamp: datetime, activity: str):
        super().__init__(timeline, timestamp)
        self._activity = activity

    @property
    def activity(self):
        return self._activity

    def as_dict(self):
        return dict(**super().as_dict(), **{
            "type": "stop",
            "activity": self._activity
        })

    @classmethod
    def from_json(cls, dct):
        return cls(
                timeline=int(dct["timeline"]),
                timestamp=datetime.strptime(dct["timestamp"], DATE_FORMAT),
                activity=str(dct["activity"]))

    def pretty_print(self):
        return "stop\n{0}".format(self._activity)


class PauseEntry(Entry):
    def __init__(self, timeline: int, timestamp: datetime):
        super().__init__(timeline, timestamp)

    def as_dict(self):
        return dict(**super().as_dict(), **{
            "type": "pause"
        })

    @classmethod
    def from_json(cls, dct):
        return cls(
                timeline=int(dct["timeline"]),
                timestamp=datetime.strptime(dct["timestamp"], DATE_FORMAT))

    def pretty_print(self):
        return "pause"


class HaltEntry(Entry):
    def __init__(self, timeline: int, timestamp: datetime):
        super().__init__(timeline, timestamp)

    def as_dict(self):
        return dict(**super().as_dict(), **{
            "type": "halt"
        })

    @classmethod
    def from_json(cls, dct):
        return cls(
                timeline=int(dct["timeline"]),
                timestamp=datetime.strptime(dct["timestamp"], DATE_FORMAT))

    def pretty_print(self):
        return "halt"


class NoteEntry(Entry):
    def __init__(self, timeline: int, timestamp: datetime, note: str):
        super().__init__(timeline, timestamp)
        self._note = note

    @property
    def note(self):
        return self._note

    def as_dict(self):
        return dict(**super().as_dict(), **{
            "type": "note",
            "note": self._note
        })

    @classmethod
    def from_json(cls, dct):
        return cls(
                timeline=int(dct["timeline"]),
                timestamp=datetime.strptime(dct["timestamp"], DATE_FORMAT),
                note=str(dct["note"]))

    def pretty_print(self):
        return "note\n" + self._note


class PropertyEntry(Entry):
    def __init__(self, timeline: int, timestamp: datetime,
                 name: str, value: Any):
        super().__init__(timeline, timestamp)
        self._name = name
        self._value = value

    @property
    def name(self):
        return self._name

    @property
    def value(self):
        return self._value

    def as_dict(self):
        return dict(**super().as_dict(), **{
            "type": "property",
            "name": self._name,
            "value": self._value
        })

    @classmethod
    def from_json(cls, dct):
        timeline = dct["timeline"]
        if timeline is not None:
            timeline = int(timeline)
        return cls(
                timeline=timeline,
                timestamp=datetime.strptime(dct["timestamp"], DATE_FORMAT),
                name=str(dct["name"]),
                value=dct["value"])

    def pretty_print(self):
        return "property\n{0} = {1}".format(self._name, self._value)


class PurgeEntry(Entry):
    def __init__(self, timeline: int, timestamp: datetime):
        super().__init__(timeline, timestamp)

    def as_dict(self):
        return dict(**super().as_dict(), **{
            "type": "purge"
        })

    @classmethod
    def from_json(cls, dct):
        return cls(
                timeline=int(dct["timeline"]),
                timestamp=datetime.strptime(dct["timestamp"], DATE_FORMAT))

    def pretty_print(self):
        return "purge"


class MarkEntry(Entry):
    def __init__(self, timeline: int, timestamp: datetime):
        super().__init__(timeline, timestamp)

    def as_dict(self):
        return dict(**super().as_dict(), **{
            "type": "mark"
        })

    @classmethod
    def from_json(cls, dct):
        return cls(
                timeline=int(dct["timeline"]),
                timestamp=datetime.strptime(dct["timestamp"], DATE_FORMAT))

    def pretty_print(self):
        return "mark"


__handlers = {
    "start": StartEntry,
    "stop": StopEntry,
    "pause": PauseEntry,
    "halt": HaltEntry,
    "note": NoteEntry,
    "property": PropertyEntry,
    "purge": PurgeEntry,
    "mark": MarkEntry
}


def __entry_handler(dct):
    if ("_nature" in dct
            and dct["_nature"] == "fyi.ajf.tmln.entry"
            and "type" in dct
            and dct["type"] in __handlers):
        return __handlers[dct["type"]].from_json(dct)
    return dct


def load(fp):
    return json.load(fp, object_hook=__entry_handler)


def loads(s):
    return json.loads(s, object_hook=__entry_handler)
