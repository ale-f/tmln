import argparse

from ..filter import select_timelines_from
from ..timeline import ConsistencyError
from .utils import print_entries

from .arg_types import fail, int_or, expression_or


def contribute_args(subparsers):
    archive_args = subparsers.add_parser(
            "archive-timeline",
            aliases=["archive"],
            help="Archive a timeline, hiding it from the list of"
                 " active timelines.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Archive a timeline, hiding it from the list of active timelines. (Archiving a
timeline is equivalent to setting its ".tmln.archived" property to true.)""")
    archive_args.set_defaults(func=cmd_archive)
    archive_args.set_defaults(archive=True)
    archive_args.add_argument(
            "id",
            nargs="*",
            type=expression_or(int_or(fail(
                    "expected timeline ID or filter expression"))),
            metavar="ID")

    unarchive_args = subparsers.add_parser(
            "unarchive-timeline",
            aliases=["unarchive"],
            help="Unarchive a timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Unarchive a timeline. (Unarchiving a timeline is equivalent to setting its
".tmln.archived" property to null.)""")
    unarchive_args.set_defaults(func=cmd_archive)
    unarchive_args.set_defaults(archive=False)
    unarchive_args.add_argument(
            "id",
            nargs="*",
            type=expression_or(int_or(fail(
                    "expected timeline ID or filter expression"))),
            metavar="ID")


def cmd_archive(tl, args):
    ts = args.timestamp
    stls = select_timelines_from(tl, *args.id)

    for stl in stls:
        try:
            print_entries(stl.property(args.timestamp, ".tmln.archived",
                                       True if args.archive else None))
            if args.archive:
                print("Archived timeline {0}.".format(stl.id))
            else:
                print("Unarchived timeline {0}.".format(stl.id))
        except ConsistencyError as e:
            print(e)
