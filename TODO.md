* Pseudoproperties: `id`, `duration`, `committed_duration`
* Automatic conversion from timespec strings like `2h4m3s` to seconds?
* Alias support?
  * `tmln alias redmine-open --shell xdg-open "https://{.redmine.endpoint}/issues/{redmine.issue}"`
* Batch property updates:
  * Support expressions in `tmln property`
  * Support the special value @property_name to assign to one property from
    another
* Better error handling in `tmln redmine`:
  * Don't crash spectacularly if an issue or project can't be looked up (see
    `redmine.exceptions.ResourceNotFoundError` and `.ForbiddenError`)
* `tmln redmine sync`
  * Add a `--threshold DURATION` option: activities shorter than `DURATION`
    should be skipped
  * Support for only synchronising specific timelines via expressions
  * Support for automatically archiving closed issues' timelines
  * Also detect timelines with paused activities in the given period
  * Bugfixing: does the timeline summary actually work?
* Expressions everywhere in general
* Chaining:
  * Better timestamp behaviour
    * `tmln --backdate start new \; property --set label hi \; property --set project yo` should produce three timeline events all at the same backdated point
  * Transactions
    * The log file should be opened and updated precisely once (perhaps with a prompt?)
* Cache file
  * Avoid replaying the log except when necessary
