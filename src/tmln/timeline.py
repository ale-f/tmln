from abc import ABC, abstractmethod
from sys import stderr
from enum import Enum
from typing import Sequence, Optional
import pickle
from pathlib import Path
from datetime import timedelta

from . import entry
from .utils import datetime_now


class ConsistencyError(Exception):
    pass


class TimelineBase(ABC):
    @property
    @abstractmethod
    def entries(self) -> Sequence[entry.Entry]:
        """Returns a (possibly empty sequence of) all of the entries in this
        timeline."""

    @property
    def last_entry(self) -> Optional[entry.Entry]:
        """Returns the last entry in this timeline, or None if this timeline is
        empty."""
        entries = self.entries
        return entries[-1] if entries else None


class GlobalTimeline(TimelineBase):
    def __init__(self, path: Path):
        self._path = path
        self._entries = []
        self._specifics = {}
        self._active = None
        self._properties = {}

    @classmethod
    def from_path(cls, path):
        if not isinstance(path, Path):
            path = Path(path)

        cache_file = path.with_suffix(".cache")
        if cache_file.exists():
            if cache_file.stat().st_mtime >= path.stat().st_mtime:
                with cache_file.open("rb") as fp:
                    try:
                        return pickle.load(fp)
                    except Exception:
                        print("ignoring invalid cache file")
            else:
                print("ignoring stale cache file")

        gt = GlobalTimeline(path)
        idents: Set[int] = set()
        try:
            with path.open("rt") as fp:
                for index, line in enumerate(fp):
                    e = entry.loads(line)
                    print("\rReplaying timeline... ({0}, {1})".format(
                            index + 1, e.timestamp), end="", file=stderr)
                    gt.apply_entry(e, _write=False)
                    if isinstance(e, entry.PurgeEntry):
                        idents.remove(e.timeline)
                    else:
                        idents.add(e.timeline)
            print("\nReplayed timeline.", file=stderr)
        except FileNotFoundError:
            pass

        [gt.get_specific_timeline(ident) for ident in idents]
        return gt

    @property
    def entries(self):
        return self._entries

    @property
    def active_timeline(self):
        return (self.get_specific_timeline(self._active)
                if self._active is not None else None)

    @property
    def all_specific_timelines(self):
        return self._specifics.values()

    @property
    def specific_timelines(self):
        return [stl for stl in self.all_specific_timelines
                if not stl.get_property(".tmln.archived", False)]

    def get_specific_timeline(
            self, ident: int, create: bool = True) -> 'SpecificTimeline':
        if ident is not None and ident not in self._specifics and create:
            self._specifics[ident] = SpecificTimeline(self, ident)
        return self._specifics.get(ident)

    def get_new_specific_timeline(self) -> 'SpecificTimeline':
        try:
            next = max(self._specifics.keys()) + 1
        except ValueError:
            next = 0
        return self.get_specific_timeline(next)

    def apply_entry(self, e, _write=True):
        self._pre_apply(e)
        self._apply(e, _write)
        self._post_apply(e)
        return e

    def _pre_apply(self, e):
        if isinstance(e, entry.StartEntry):
            if self._active is not None:
                raise ConsistencyError(
                        ("can't start timeline {0}," +
                         " timeline {1} already in progress").format(
                                e.timeline, self._active))
        elif isinstance(e, entry.StopEntry):
            if self._active is None:
                raise ConsistencyError("no timeline in progress")
        elif isinstance(e, entry.PauseEntry):
            if self._active is None:
                raise ConsistencyError("no timeline in progress")
        elif isinstance(e, entry.HaltEntry):
            if self._active is None:
                raise ConsistencyError("no timeline in progress")
        elif isinstance(e, entry.PurgeEntry):
            if self._active == e.timeline:
                raise ConsistencyError("can't purge active timeline")
        if self._entries:
            last = self._entries[-1]
            if e.timestamp < last.timestamp:
                raise ConsistencyError(
                        ("timestamp {0} is earlier " +
                         "than previous timestamp {1}").format(
                                e.timestamp, last.timestamp))

    def _apply(self, e, write=True):
        if write:
            with self._path.open("at") as fp:
                e.save(fp)
                fp.write("\n")
        self._entries.append(e)

    def _post_apply(self, e):
        if isinstance(e, entry.PurgeEntry):
            tl_id = e.timeline
            self._entries = list(
                    e for e in self._entries if e.timeline != tl_id)
            del self._specifics[tl_id]
        elif isinstance(e, entry.PropertyEntry) and e.timeline is None:
            key = e.name
            value = e.value
            if value is not None:
                self._properties[key] = value
            elif key in self._properties:
                del self._properties[key]
        else:
            if isinstance(e, entry.StartEntry):
                self._active = e.timeline
            elif isinstance(e, (
                    entry.StopEntry, entry.PauseEntry, entry.HaltEntry)):
                self._active = None
            self.get_specific_timeline(e.timeline)._apply_entry(e)
        with self._path.with_suffix(".cache").open("wb") as fp:
            pickle.dump(self, fp)

    @property
    def properties(self):
        return self._properties

    def get_property(self, key, default=None):
        return self.properties.get(key, default)

    def property(self, timestamp, key, value):
        return self.apply_entry(
                entry.PropertyEntry(None, timestamp, key, value))


class SpecificTimeline(TimelineBase):
    def __init__(self, parent: GlobalTimeline, id: int):
        self._parent = parent
        self._id = id

        self._start = None
        self._tasks = []
        self._parts = []
        self._properties = {}

        for entry in self.entries:
            self._apply_entry(entry)

    @property
    def parent(self):
        return self._parent

    @property
    def id(self):
        return self._id

    @property
    def entries(self):
        return [e for e in self.parent.entries
                if e.timeline == self.id]

    def has_parts(self):
        return bool(self._parts)

    @property
    def tasks(self):
        return self._tasks

    @property
    def last_task(self):
        tasks = self.tasks
        return tasks[-1] if tasks else None

    @property
    def start_entry(self):
        return self._start

    @property
    def duration(self) -> timedelta:
        total = timedelta()
        for _, duration in self._parts:
            total += duration
        for _, _, duration in self.tasks:
            total += duration
        if self._start:
            total += datetime_now() - self._start.timestamp
        return total

    def _apply_entry(self, e):
        if isinstance(e, entry.StartEntry):
            self._start = e
        elif (isinstance(e, entry.StopEntry)
                or isinstance(e, entry.PauseEntry)):
            active_since = self._start.timestamp
            self._parts.append(
                    (active_since.date(), e.timestamp - active_since))
            if isinstance(e, entry.StopEntry):
                activity = e.activity
                for start, duration in self._parts:
                    self._tasks.append((start, activity, duration))
                self._parts = []
            self._start = None
        elif isinstance(e, entry.HaltEntry):
            self._parts = []
            self._start = None
        elif isinstance(e, entry.NoteEntry):
            pass
        elif isinstance(e, entry.PropertyEntry):
            key = e.name
            value = e.value
            if value is not None:
                self._properties[key] = value
            elif key in self._properties:
                del self._properties[key]

    @property
    def properties(self):
        return self._properties

    def get_property(self, key, default=None):
        return self.properties.get(
                key, self.parent.get_property(key, default))

    def start(self, timestamp):
        return self.parent.apply_entry(
                entry.StartEntry(self.id, timestamp))

    def stop(self, timestamp, activity):
        return self.parent.apply_entry(
                entry.StopEntry(self.id, timestamp, activity))

    def pause(self, timestamp):
        return self.parent.apply_entry(
                entry.PauseEntry(self.id, timestamp))

    def halt(self, timestamp):
        return self.parent.apply_entry(
                entry.HaltEntry(self.id, timestamp))

    def note(self, timestamp, note):
        return self.parent.apply_entry(
                entry.NoteEntry(self.id, timestamp, note))

    def property(self, timestamp, key, value):
        return self.parent.apply_entry(
                entry.PropertyEntry(self.id, timestamp, key, value))

    def purge(self, timestamp):
        return self.parent.apply_entry(
                entry.PurgeEntry(self.id, timestamp))

    def mark(self, timestamp):
        return self.parent.apply_entry(
                entry.MarkEntry(self.id, timestamp))
