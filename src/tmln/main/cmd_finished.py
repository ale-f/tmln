import argparse

from .utils import print_status


def contribute_args(subparsers):
    finished_args = subparsers.add_parser(
            "stop-activity",
            aliases=["finished"],
            help="Conclude an activity and continue"
                 " working on the current timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Conclude an activity and continue working on the current timeline.

(See also the stop-timeline subcommand, which concludes an activity and stops
working on the current timeline.)""")
    finished_args.set_defaults(func=cmd_finished)
    finished_args.add_argument(
            "activity",
            help="a text description of the concluded activity",
            metavar="ACTIVITY")


def cmd_finished(tl, args):
    ts = args.timestamp
    stl = tl.active_timeline
    if stl:
        stl.stop(args.timestamp, args.activity)
        print_status(stl)
        stl.start(args.timestamp)
        print("Work on timeline {0} continues at {1}.".format(stl.id, ts))
    else:
        print("No active timeline.")
