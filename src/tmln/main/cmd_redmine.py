import re
from math import ceil
from typing import Dict, Tuple
import argparse
from urllib.parse import urlsplit
from datetime import date
from dateutil.parser import parse as parse_datetime
import subprocess

try:
    from redminelib import Redmine
except ImportError:
    from redmine import Redmine

from ..filter import Filter
from .arg_types import expression_or
from .utils import print_status


def url(s):
    scheme, netloc, path, query, fragment = urlsplit(s)
    if scheme not in ('http', 'https'):
        raise argparse.ArgumentTypeError(
                "Invalid scheme {0} in {1}".format(scheme, s))
    elif not netloc:
        raise argparse.ArgumentTypeError("Missing netloc in {1}".format(s))
    else:
        return s


def datespec(s):
    try:
        return parse_datetime(s).date()
    except ValueError as ex:
        print(ex)
        raise argparse.ArgumentTypeError(*ex.args)


def contribute_args(subparsers):
    redmine_args = subparsers.add_parser(
            "redmine",
            help="Interact with a Redmine instance.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Interact with a Redmine instance.""")
    redmine_args.add_argument(
            "--endpoint",
            type=url,
            metavar="URL",
            default=None)
    redmine_args.add_argument(
            "--api-key",
            type=str,
            metavar="KEY",
            default=None)
    redmine_subparsers = redmine_args.add_subparsers(
            dest="subcommand",
            title="subcommands",
            required=True)

    sync_args = redmine_subparsers.add_parser(
            "sync",
            help="Synchronise timeline tasks with a Redmine instance.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Synchronise timeline tasks with a Redmine instance.""")
    sync_args.set_defaults(func=cmd_redmine_sync)
    sync_args.add_argument(
            "--project-property",
            type=str,
            default="redmine.project")
    sync_args.add_argument(
            "--issue-property",
            type=str,
            default="redmine.issue")
    sync_args.add_argument(
            "--period-size",
            type=int,
            default=180)
    sync_args.add_argument(
            "--merge",
            action="store_true")
    sync_args.add_argument(
            "start",
            type=datespec,
            metavar="START")
    sync_args.add_argument(
            "end",
            type=datespec,
            metavar="END",
            default=date.max,
            nargs="?")

    open_args = redmine_subparsers.add_parser(
            "open",
            help="""
Opens the Redmine issue connected with one or more timelines in a browser.""",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Opens the Redmine issue connected with one or more timelines in a browser.""")
    open_args.set_defaults(func=cmd_redmine_open)
    open_args.add_argument(
            "id",
            metavar="expr|ID",
            nargs="*",
            type=expression_or(int))


def yield_all_tasks_between(tl, start, end, merge=False):
    def _select(tl, start, end):
        for stl in tl.all_specific_timelines:
            for task_start, activity, duration in stl.tasks:
                if task_start < start or task_start > end:
                    continue
                yield (stl, task_start, activity, duration)
    if not merge:
        yield from _select(tl, start, end)
    else:
        merged: Dict[Tuple[date, str], float] = {}
        for stl, task_start, activity, duration in _select(tl, start, end):
            key = (stl, task_start, activity)
            if key not in merged:
                merged[key] = duration
            else:
                merged[key] += duration
        for (stl, task_start, activity), duration in merged.items():
            yield (stl, task_start, activity, duration)


def cmd_redmine_sync(tl, args):
    args.endpoint = args.endpoint or tl.get_property(".redmine.endpoint")
    args.api_key = args.api_key or tl.get_property(".redmine.api_key")

    if not args.endpoint:
        print("Missing endpoint.")
        return 1
    if not args.api_key:
        print("Missing API key.")
        return 1

    redmine = Redmine(args.endpoint, key=args.api_key)
    authentication = redmine.auth()
    user_id = authentication.id

    # We conservatively only care about time entries that have a "[tmln]" tag
    # in their name
    initial = {entry.id for entry in redmine.time_entry.filter(
            user_id=user_id,
            from_date=args.start,
            to_date=args.end) if "[tmln]" in entry.comments}
    print(initial)
    backed = set()
    missing = set()

    for stl, start, activity, duration in yield_all_tasks_between(
            tl, args.start, args.end, merge=args.merge):
        issue = stl.get_property(args.issue_property, None)
        project = stl.get_property(args.project_property, None)
        if not (issue or project):
            missing.add(stl)
            continue
        elif project is not None and not isinstance(project, int):
            project = redmine.project.get(resource_id=project).id

        periods = ceil(duration.total_seconds() / args.period_size)
        hours = (periods * args.period_size) / 3600
        desc = activity + " [tmln]"
        print(start, issue or project, desc, hours)

        entries = redmine.time_entry.filter(
                user_id=user_id,
                issue_id=issue,
                project_id=project,
                spent_on=start,
                hours=hours,
                comments=desc)
        # It *is* possible to have multiple registrations on the same day
        # with identical properties; handle this case by only allowing each
        # existing time entry to satisfy one task
        entry = None
        for entry in entries:
            if entry.id not in backed:
                break
        if not entry:
            entry = redmine.time_entry.create(
                user_id=user_id,
                issue_id=issue,
                project_id=project,
                spent_on=start,
                hours=hours,
                comments=desc)
            print("\tcreated  {0}".format(entry.id))
        else:
            print("\texisting {0}".format(entry.id))
        backed.add(entry.id)

    orphaned_entries = initial.difference(backed)
    if orphaned_entries:
        print("Deleting orphaned entries:")
        for i in orphaned_entries:
            redmine.time_entry.delete(i)
            print("\t{0}".format(i))

    if missing:
        print("""\
The following timelines specify activities in the given period, but do not
have Redmine information:""")
        print_status(*sorted(missing, key=lambda stl: stl.id))


def cmd_redmine_open(tl, args):
    args.endpoint = args.endpoint or tl.get_property(".redmine.endpoint")

    if not args.endpoint:
        print("Missing endpoint.")
        return 1

    if args.id:
        collected_timelines = set()
        guaranteed_timelines = set()
        for ident in args.id:
            if isinstance(ident, Filter):
                collected_timelines.update(
                        stl for stl in tl.all_specific_timelines
                        if ident.accepts(stl))
            else:
                stl = tl.get_specific_timeline(ident, create=False)
                if stl:
                    guaranteed_timelines.add(stl)
        selected_timelines = collected_timelines.union(guaranteed_timelines)
    else:
        if tl.active_timeline:
            selected_timelines = [tl.active_timeline]
        else:
            print("No active timeline.")
            return 1

    for tl in selected_timelines:
        issue = tl.get_property("redmine.issue")
        if issue:
            subprocess.run(
                    ["xdg-open",
                     "{0}/issues/{1}".format(args.endpoint, issue)])
