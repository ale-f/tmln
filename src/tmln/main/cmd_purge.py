import argparse

from ..timeline import ConsistencyError
from .utils import print_entries


def contribute_args(subparsers):
    purge_args = subparsers.add_parser(
            "purge-timeline",
            aliases=["purge"],
            help="Purge a timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Purge a timeline, completely removing its entries from the global timeline.""")
    purge_args.set_defaults(func=cmd_purge)
    purge_args.add_argument("id", type=int, metavar="ID")


def cmd_purge(tl, args):
    ts = args.timestamp
    stl = tl.get_specific_timeline(args.id, create=False)

    if stl:
        try:
            print_entries(stl.purge(ts))
            print("Purged timeline {0}.".format(args.id))
        except ConsistencyError as e:
            print(e)
    else:
        print("Can't purge empty timeline {0}.".format(args.id))
