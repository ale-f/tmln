# `tmln`

`tmln` is a realistic timeline management tool designed for flexibility.

Getting started
--

Our story starts, like most stories, at the beginning. Actually, we start at
_several_ beginnings:

* it's 8:56am and the working day is about to start; and
* the `tmln` log is empty.

Let's fix the latter of these straight away:

```
$ tmln start new
Work on timeline 0 begins at 2019-11-18 08:56:50+01:00.
```

We now have one entry in the log: we've started work on a new timeline.
Exciting! Admittedly, we don't yet know what this timeline _represents_, but
that's fine; we'll get around to that eventually.

We spend five minutes looking at the agile board and find something worth
looking at. OK; now that we've finally decided what we're working on, we can
note its number in the bug tracker down in the log as a _property_:

```
$ tmln property --set tracker_nr 1218
timestamp                    id  entry
-------------------------  ----  ---------------
2019-11-18 09:01:55+01:00     0  property
                                 tracker_nr = 1218
```

`tmln` will often print little summaries of what's happened or of entries
that have just been appended to the log. Here's one of them; we see that an
entry of type `property` has been appended to timeline 0, along with the
property's name and value.

With a few exceptions (that we'll come to soon), `tmln` doesn't really care
about the content of properties, so we can use them to store any information
that's relevant for us. For example, we might also want to add a label to this
timeline so that we don't forget what it is &mdash; bug tracker numbers aren't
terribly human-memorable!

```
$ tmln property --set label "NXR export performance"
timestamp                    id  entry
-------------------------  ----  ---------------
2019-11-18 09:02:10+01:00     0  property
                                 label = NXR export performance
```

OK. Having set that up, we knuckle down and get to work.

After an hour, we've worked out what's wrong with the performance of our NXR
exporter (whatever one of _those_ is). We should probably register this in the
log somehow; the customary way to do that is to _stop an activity_.

```
$ tmln stop-activity "Diagnosed performance issue"
  id  label                     tracker_nr  duration    last_task
----  ----------------------  ------------  ----------  ----------------------------------------------
   0  NXR export performance          1218  1h6m29s     Diagnosed performance issue (2019-11-18, 1h6m)
Work on timeline 0 continues at 2019-11-18 10:03:19+01:00.
```

The log now contains an entry for an _activity_, a block of time with an
associated comment. In this case, the comment observes that we spent this time
working out what was causing the performance issue.

Note that the `stop-activity` command doesn't stop the clock: we've concluded
one block, but we've started a new one immediately afterward. Which is just as
well &mdash; after all, we've only _diagnosed_ the performance issue. We still
need to _fix_ the blasted thing...

An hour and a half more work and we've arrived at a good solution. In goes
another activity:

```
$ tmln stop-activity "Implemented cache"
  id  label                     tracker_nr  duration    last_task
----  ----------------------  ------------  ----------  ---------------------------------------
   0  NXR export performance          1218  2h37m33s    Implemented cache (2019-11-18, 1h31m4s)
Work on timeline 0 continues at 2019-11-18 11:34:23+01:00.
```

Right. It's already half-past eleven, so we probably still have time to update
the bug tracker and to write up a MR (or a PR, if we feel so inclined) before
lunch. Our theoretical company's policy is that fixes for performance problems
need to come with _hard numbers_, so we run some representative tests on the
old and new versions of the code: the new version is done in five seconds, but
the old one takes almost fifteen minutes...

```
$ tmln stop-activity "Performance comparisons for MR"
  id  label                     tracker_nr  duration    last_task
----  ----------------------  ------------  ----------  --------------------------------------------------
   0  NXR export performance          1218  2h52m42s    Performance comparisons for MR (2019-11-18, 15m9s)
Work on timeline 0 continues at 2019-11-18 11:49:32+01:00.
```

The building is beginning to fill with the delicious smell of freshly-baked
bread as the cafeteria sets up for lunch, which gives us the burst of energy we
need to write up the MR in _precisely_ five minutes:

```
$ tmln stop-timeline "Opened MR"
  id  label                     tracker_nr  duration    last_task
----  ----------------------  ------------  ----------  --------------------------
   0  NXR export performance          1218  2h57m42s    Opened MR (2019-11-18, 5m)
Work on timeline 0 stops at 2019-11-18 11:54:32+01:00.
```

It's time for lunch, so this time we use the `stop-timeline` command, which
finishes activity without starting a new one. Mission completed and stomach
rumbling, we head for the cafeteria.

Back to work
--

Half an hour (and a pretty great sandwich/soup combo) later, we return to the
office and pick the day's next task.

```
$ tmln start new
Work on timeline 1 begins at 2019-11-18 12:24:32+01:00.
$ tmln property --set tracker_nr 1240
timestamp                    id  entry
-------------------------  ----  -----------------
2019-11-18 12:26:51+01:00     1  property
                                 tracker_nr = 1240
$ tmln property --set label "Corrupted uploads?"
timestamp                    id  entry
-------------------------  ----  --------------------------
2019-11-18 12:27:16+01:00     1  property
                                 label = Corrupted uploads?
```

But things don't go so smoothly this time: five minutes into debugging, our
colleague leans over and asks if we know anything about `flob3.js`,
Twooglebook's latest (and allegedly greatest) UI framework. We do, as it
happens... so we find ourselves helping them debug a problem in the mobile
version of their project for the next forty-five minutes.

The resulting warm glow of satisfaction is all very well, but our timeline now
has a problem. We have forty-five minutes of time logged against issue #1240
during which we, er, weren't actually working on that.

```
$ tmln status
Active timeline is 1 (for 52m46s).
  id  label                     tracker_nr  duration    last_task
----  ----------------------  ------------  ----------  --------------------------
   0  NXR export performance          1218  2h57m42s    Opened MR (2019-11-18, 5m)
   1  Corrupted uploads?              1240  52m46s      (none)
```

Can we recover from this?

The answer is yes, with a bit of light time travel:

```
$ tmln --delta=-45m status
Active timeline is 1 (for 8m).
  id  label                     tracker_nr  duration    last_task
----  ----------------------  ------------  ----------  --------------------------
   0  NXR export performance          1218  2h57m42s    Opened MR (2019-11-18, 5m)
   1  Corrupted uploads?              1240  8m          (none)
```

`tmln`'s `--delta` command-line argument lets you adjust the definition of
"now". We're now back at the start of our detour, so we can use the `pause`
command to _interrupt_ that timeline:

```
$ tmln --delta=-45m pause
Paused timeline 1 at 2019-11-18 12:32:45+01:00.
$ tmln status
No active timeline.
  id  label                     tracker_nr  duration    last_task
----  ----------------------  ------------  ----------  --------------------------
   0  NXR export performance          1218  2h57m42s    Opened MR (2019-11-18, 5m)
   1  Corrupted uploads?              1240  8m13s       (none)
```

If we start a new timeline now, there'll be a hole of a few seconds, which
isn't a _big_ problem... but which is a bit untidy. Luckily, `tmln` has another
time travel option, `--backdate`, to seal up that hole completely:

```
$ tmln --backdate start new
Work on timeline 2 begins at 2019-11-18 12:32:45+01:00.
```

We ping our colleague to get a bug tracker number, and we get one, which we
promptly record in the log:

```
$ tmln property --set tracker_nr 1222
timestamp                    id  entry
-------------------------  ----  -----------------
2019-11-18 13:19:45+01:00     2  property
                                 tracker_nr = 1222
$ tmln property --set label "flob3 debugging help"
timestamp                    id  entry
-------------------------  ----  ----------------------------
2019-11-18 13:20:19+01:00     2  property
                                 label = flob3 debugging help
```

We didn't to use `--backdate` or `--delta` this time, but that's not a problem:
we're going to report this as a single activity that just finished, and then
it's back to the data corruption bug.

```
$ tmln stop-timeline "Helped debug flob3 ReferenceErrors"
  id  label                   tracker_nr  duration    last_task
----  --------------------  ------------  ----------  ------------------------------------------------------
   2  flob3 debugging help          1222  48m4s       Helped debug flob3 ReferenceErrors (2019-11-18, 48m4s)
Work on timeline 2 stops at 2019-11-18 13:20:49+01:00.
$ tmln --backdate start 1
Work on suspended timeline 1 resumes at 2019-11-18 13:20:49+01:00.
```
