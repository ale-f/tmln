import argparse


def contribute_args(subparsers):
    halt_args = subparsers.add_parser(
            "halt-timeline",
            aliases=["halt", "abort"],
            help="Stop working on the current timeline"
                 " without concluding an activity.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Stop working on the current timeline without concluding an activity. Recorded
time not yet assigned to a task will be discarded.""")
    halt_args.set_defaults(func=cmd_halt)


def cmd_halt(tl, args):
    ts = args.timestamp
    stl = tl.active_timeline

    if stl:
        stl.halt(ts)
        print("Halted timeline {0} at {1}.".format(stl.id, ts))
    else:
        print("No active timeline.")
