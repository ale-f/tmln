import argparse

from ..timeline import ConsistencyError
from ..utils import json_to_str, str_to_json
from .utils import print_entries, print_tabulate
from .arg_types import int_or


def contribute_args(subparsers):
    property_args = subparsers.add_parser(
            "property",
            help="Get or set timeline metadata.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Get or set timeline metadata.""")
    property_args.set_defaults(func=cmd_property)
    property_args.add_argument(
            "id",
            metavar="ID|global",
            nargs="?",
            help="the timeline to operate on, if not the current one",
            type=int_or("global"))
    property_mode = property_args.add_mutually_exclusive_group(required=True)
    property_mode.add_argument(
            "--get",
            help="print the value of the named property %(metavar)s",
            dest="get_key",
            metavar="KEY")
    property_mode.add_argument(
            "--set",
            metavar=("KEY", "VALUE"),
            help="set the value of the named property KEY to VALUE (a"
                 " VALUE of null will delete the property)",
            dest="set_pair",
            nargs=2)
    property_mode.add_argument(
            "--dump",
            help="print all the names and values of the timeline's properties",
            action="store_true")
    property_mode.add_argument(
            "--list",
            help="list all the names of the timeline's properties",
            action="store_true")


def cmd_property(tl, args):
    ts = args.timestamp
    if args.id is None:
        stl = tl.active_timeline
    elif args.id == "global":
        stl = tl
    else:
        stl = tl.get_specific_timeline(args.id, create=False)

    if stl:
        if args.get_key:
            key = args.get_key
            p = stl.get_property(key)
            if p is None:
                exit(1)
            else:
                print(json_to_str(p))
        elif args.set_pair:
            key, value = args.set_pair
            try:
                print_entries(stl.property(ts, key, str_to_json(value)))
            except ConsistencyError as e:
                print(e)
        elif args.dump:
            lines = [["name", "value"]]
            for k, v in sorted(stl.properties.items(), key=lambda i: i[0]):
                if v is None:
                    continue
                else:
                    lines.append([k, json_to_str(v)])
            print_tabulate(lines)
        elif args.list:
            for k in sorted(stl.properties.keys()):
                print(k)
    else:
        print("No active timeline." if args.id is None
              else "No such timeline.")
