import argparse

from ..filter import Filter, select_timelines_from
from ..utils import delta_to_timespec
from .utils import print_status

from .arg_types import fail, int_or, expression_or


def contribute_args(subparsers):
    status_args = subparsers.add_parser(
            "list-timelines",
            aliases=["timelines"],
            help="List and summarise all of the active timelines.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
List and summarise all of the active timelines. (An active timeline is one for
which timeline entries exist and that has not been archived.)""")
    status_args.add_argument(
            "id",
            metavar="expr|ID",
            nargs="*",
            type=expression_or(int_or(fail(
                    "expected timeline ID or filter expression"))))
    status_args.set_defaults(func=cmd_status, simple=False)

    status_args = subparsers.add_parser(
            "status",
            help="Print and summarise the current active timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Print and summarise the current active timeline, if there is one.""")
    status_args.set_defaults(func=cmd_status, simple=True)


def filter(it, pred):
    for i in it:
        if pred(i):
            yield i


def cmd_status(tl, args):
    ts = args.timestamp
    active = tl.active_timeline

    if active is not None:
        duration = ts - active.start_entry.timestamp
        print("Active timeline is {0} (for {1}).".format(
                active.id, delta_to_timespec(duration)))
        if args.simple:
            print_status(active)
    else:
        print("No active timeline.")

    if not args.simple:
        if args.id:
            summary_timelines = select_timelines_from(tl, *args.id)
        else:
            summary_timelines = tl.specific_timelines
        print_status(*sorted(summary_timelines, key=lambda stl: stl.id))
