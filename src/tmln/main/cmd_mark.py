import argparse

from ..timeline import ConsistencyError
from .utils import print_status


def contribute_args(subparsers):
    mark_args = subparsers.add_parser(
            "mark",
            help="Place a contentless marker on the current timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Place a contentless marker on the current timeline, chiefly useful for
influencing later uses of the --backdate argument.""")
    mark_args.set_defaults(func=cmd_mark)


def cmd_mark(tl, args):
    ts = args.timestamp
    stl = tl.active_timeline

    if stl:
        try:
            stl.mark(args.timestamp)
            print(f"Timeline {stl.id} marked at {ts}.")
        except ConsistencyError as e:
            print(e)
    else:
        print("No active timeline.")
