import argparse

from ..timeline import ConsistencyError
from .utils import print_status


def contribute_args(subparsers):
    stop_args = subparsers.add_parser(
            "stop-timeline",
            aliases=["stop"],
            help="Conclude an activity and"
                 " stop working on the current timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Conclude an activity and stop working on the current timeline.

(See also the stop-activity subcommand, which concludes an activity without
stopping work on the current timeline.)""")
    stop_args.set_defaults(func=cmd_stop)
    stop_args.add_argument(
            "activity",
            help="a text description of the concluded activity",
            metavar="ACTIVITY")


def cmd_stop(tl, args):
    ts = args.timestamp
    stl = tl.active_timeline

    if stl:
        try:
            stl.stop(args.timestamp, args.activity)
            print_status(stl)
            print("Work on timeline {0} stops at {1}.".format(stl.id, ts))
        except ConsistencyError as e:
            print(e)
    else:
        print("No active timeline.")
