import argparse


def contribute_args(subparsers):
    pause_args = subparsers.add_parser(
            "pause-timeline",
            aliases=["pause"],
            help="Pause work on the current timeline"
                 " without concluding an activity.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Pause work on the current timeline without concluding an activity.

When an activity is eventually concluded on a timeline with pauses, multiple
activities will be created to cover all of the periods between start entries
and pause entries.""")
    pause_args.set_defaults(func=cmd_pause)


def cmd_pause(tl, args):
    ts = args.timestamp
    stl = tl.active_timeline

    if stl:
        stl.pause(ts)
        print("Paused timeline {0} at {1}.".format(stl.id, ts))
    else:
        print("No active timeline.")
