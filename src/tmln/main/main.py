#!/usr/bin/env python3

from os import getenv, getuid, makedirs
from pwd import getpwuid
from sys import argv, exit
from pathlib import Path
import argparse
from datetime import timedelta
import readline  # noqa


from ..utils import datetime_now
from ..timeline import GlobalTimeline
from .arg_types import pathspec, timespec
from . import cmd_activities
from . import cmd_archive
from . import cmd_entries
from . import cmd_finished
from . import cmd_halt
from . import cmd_mark
from . import cmd_note
from . import cmd_pause
from . import cmd_property
from . import cmd_purge
from . import cmd_redmine
from . import cmd_start
from . import cmd_status
from . import cmd_stop


def split_seq(seq, delim=";"):
    groups = []
    while seq:
        try:
            split_at = seq.index(delim)
            groups.append(seq[:split_at])
            seq = seq[split_at + 1:]
        except ValueError:
            break
    groups.append(seq)
    return groups


def main():
    now = datetime_now()

    parser = argparse.ArgumentParser()
    parser.add_argument(
            "--path",
            type=pathspec,
            help="the path to the timeline file",
            default=None)
    parser.add_argument(
            "--delta",
            dest='delta',
            metavar="DELTA",
            type=timespec,
            help="a delta to be applied to the base timestamp, for"
                 " example '-15m' or '[+]8h'",
            default=timedelta(0))
    parser.add_argument(
            "--backdate",
            help="set the base timestamp to the timestamp of"
                 " the last entry (default: the current time)",
            dest='timestamp',
            default=now,
            action='store_const',
            const='backdate')
    subparsers = parser.add_subparsers(
            dest="command",
            title="subcommands",
            required=True)

    for subcommand_module in (
            cmd_start, cmd_pause, cmd_finished, cmd_stop, cmd_halt, cmd_note,
            cmd_status, cmd_activities, cmd_entries, cmd_property,
            cmd_archive, cmd_purge, cmd_mark, cmd_redmine):
        subcommand_module.contribute_args(subparsers)

    # Parse all of the argument groups in advance so that we don't execute
    # half of an argument pipeline!
    argses = []
    for grp in split_seq(argv[1:], ";"):
        argses.append(parser.parse_args(grp))

    tl = None
    tlpath = None
    for args in argses:
        if args.path is None:
            home = getenv("HOME", getpwuid(getuid()).pw_dir)
            data_dir = getenv("XDG_DATA_HOME", home + "/.local/share")
            args.path = Path(data_dir).joinpath("fyi.ajf.tmln/default")
            makedirs(args.path.parent, exist_ok=True)
        if tlpath != args.path:
            tl = GlobalTimeline.from_path(str(args.path))
            tlpath = args.path

        if args.timestamp == 'backdate':
            all_entries = tl.entries
            if not all_entries:
                print("No last entry.")
                exit(3)
            else:
                args.timestamp = all_entries[-1].timestamp
        args.timestamp += args.delta

        rv = args.func(tl, args)
        if rv is not None:
            exit(rv)


if __name__ == '__main__':
    main()
