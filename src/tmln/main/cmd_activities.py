import argparse

from ..utils import delta_to_timespec
from .utils import print_tabulate


def contribute_args(subparsers):
    activities_args = subparsers.add_parser(
            "list-activities",
            aliases=["activities"],
            help="List all of the completed activities on a timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
List all of the completed activities on a timeline. (Periods during which a
timeline has been paused will not be listed until they're included in a
completed activity.)""")
    activities_args.add_argument(
            "id",
            metavar="ID",
            nargs="?",
            type=int)
    activities_args.set_defaults(func=cmd_activities)


def cmd_activities(tl, args):
    if args.id is None:
        stl = tl.active_timeline
    else:
        stl = tl.get_specific_timeline(args.id, create=False)

    if stl:
        lines = [["start", "activity", "duration"]]
        for start, activity, duration in stl.tasks:
            lines.append([start, activity, delta_to_timespec(duration)])
        print_tabulate(lines)
    else:
        print("No active timeline." if args.id is None
              else "No such timeline.")
