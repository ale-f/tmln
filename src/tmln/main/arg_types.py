from pathlib import Path
import argparse
import functools

from ..utils import timespec_to_delta
from ..filter import from_string as make_filter


def pathspec(s):
    p = Path(s)
    if not p.parent.exists():
        raise argparse.ArgumentTypeError(
                "path '{0}' is not reachable".format(s))
    return p


def timespec(s):
    m = timespec_to_delta(s)
    if m is not None:
        return m
    else:
        raise argparse.ArgumentTypeError(
                "'{0}' is not a valid timestamp specification".format(s))


def maybe_call(c, v):
    if callable(c):
        return c(v)
    else:
        if c != v:
            raise argparse.ArgumentTypeError(v)
        return v


def fail(msg):
    def _fail(s):
        raise argparse.ArgumentTypeError(msg)
    return _fail


def const_or(c, maybe_callable):
    def _const_or(s):
        if s == c:
            return s
        else:
            return maybe_call(maybe_callable, s)
    return _const_or


def constructor_or(c, maybe_callable):
    def _constructor_or(s):
        try:
            return c(s)
        except ValueError:
            return maybe_call(maybe_callable, s)
    return _constructor_or


int_or = functools.partial(constructor_or, int)
expression_or = functools.partial(constructor_or, make_filter)
