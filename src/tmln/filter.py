from abc import ABC, abstractmethod
from enum import Enum
from json import dumps, JSONDecoder, JSONDecodeError
import re


class Filter(ABC):
    @abstractmethod
    def accepts(self, timeline):
        pass


class DummyFilter(Filter):
    def __init__(self, what):
        self._what = bool(what)

    def accepts(self, timeline):
        return self._what


class CompoundFilter(Filter):
    def __init__(self):
        self._components = []

    def add(self, filt):
        self.components.append(filt)

    @property
    def components(self):
        return self._components

    def __str__(self):
        return "{0}({1})".format(
            type(self).__name__,
            ", ".join(str(c) for c in self.components))


class AndFilter(CompoundFilter):
    def accepts(self, timeline):
        for filter in self.components:
            if not filter.accepts(timeline):
                return False
        return True


class OrFilter(CompoundFilter):
    def accepts(self, timeline):
        for filter in self.components:
            if filter.accepts(timeline):
                return True
        return False


class PropertyFilterType(Enum):
    Equal = "=="
    NotEqual = "!="
    LessOrEqual = "<="
    GreaterOrEqual = ">="
    LessThan = "<"
    GreaterThan = ">"
    Regex = "~~"


class PropertyFilter(Filter):
    def __init__(self, property_name, operator, property_value):
        self._property_name = property_name
        self._operator = operator
        self._property_value = property_value

    def accepts(self, timeline):
        a1 = timeline.properties.get(self.property_name)
        op = self.operator
        a2 = self.property_value

        try:
            if op == PropertyFilterType.Equal:
                return a1 == a2
            elif op == PropertyFilterType.NotEqual:
                return a1 != a2
            elif op == PropertyFilterType.LessOrEqual:
                return a1 <= a2
            elif op == PropertyFilterType.GreaterOrEqual:
                return a1 >= a2
            elif op == PropertyFilterType.LessThan:
                return a1 < a2
            elif op == PropertyFilterType.GreaterThan:
                return a1 > a2
            elif op == PropertyFilterType.Regex:
                return bool(re.search(str(a2), str(a1), re.IGNORECASE))
        except TypeError:
            return False

    @property
    def property_name(self):
        return self._property_name

    @property
    def operator(self):
        return self._operator

    @property
    def property_value(self):
        return self._property_value

    def __str__(self):
        return "{0}{1}{2}".format(
                self.property_name, self.operator.value,
                dumps(self.property_value))


name_op_part = re.compile(
        r"^(?P<name>[a-zA-Z_.][.\w]*)(?P<op>==|!=|<=|>=|<|>|~~|)")


def from_string(s):
    """Compiles a query expression into a Filter object. A query expression is
    a string of the form

        name1OPvalue1[,name2OPvalue2[,name3OPvalue3, ...]]

    ... where nameN is a valid property name, OP is one of the operators listed
    in PropertyFilterType, and valueN is an arbitrary JSON expression. (OP
    and valueN can be omitted to produce a simple test that the named property
    is set.)"""
    r = AndFilter()
    jd = JSONDecoder()

    # Unpacking is a bit interesting. We use boring regex matching to pick out
    # names and operators, and once we've found those we use a JSONDecoder *in
    # raw mode* to handle the rest. The use of raw mode means that the decoder
    # will ignore trailing input instead of failing, which is perfect for this
    # use case -- trailing input is the rest of the expression!
    match = name_op_part.match(s)
    while match:
        name, op_value = match.group("name"), match.group("op")
        s = s[match.end():]

        if op_value:
            op = PropertyFilterType(op_value)
            try:
                value, end = jd.raw_decode(s)
            except JSONDecodeError:
                # If we couldn't decode this value as a JSON object, then treat
                # everything until the next comma (or the end) as a simple
                # string
                end = s.find(",")
                if end == -1:
                    end = len(s)
                value = s[:end]
        else:
            # As a special case, a bare property name with no operator after
            # it is treated as a test that the property is set
            op = PropertyFilterType.NotEqual
            value = None
            end = 0

        r.add(PropertyFilter(name, op, value))

        if end == len(s):
            break
        elif s[end] != ",":
            raise ValueError(
                "expected ',' to continue query, got '{0}'".format(s[end]))
        else:
            s = s[end + 1:]
        match = name_op_part.match(s)
    else:
        if not s:
            raise ValueError("trailing empty query fragment")
        else:
            raise ValueError(
                "didn't understand query fragment '{0}'".format(s))

    if not r.components:
        return None
    elif len(r.components) == 1:
        return r.components[0]
    else:
        return r


def select_timelines_from(tl, *filters):
    collected_timelines = set()
    guaranteed_timelines = set()
    for f in filters:
        if isinstance(f, Filter):
            collected_timelines.update(
                    stl for stl in tl.all_specific_timelines
                    if f.accepts(stl))
        else:
            stl = tl.get_specific_timeline(f, create=False)
            if stl:
                guaranteed_timelines.add(stl)
            else:
                print("{0}: no such timeline.".format(f))
    return collected_timelines.union(guaranteed_timelines)
