import argparse

from ..timeline import ConsistencyError
from .utils import print_entries


def contribute_args(subparsers):
    note_args = subparsers.add_parser(
            "add-note",
            aliases=["note"],
            help="Add a textual note to a timeline.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Add a textual note to a timeline.""")
    note_args.set_defaults(func=cmd_note)
    note_args.add_argument(
            "id",
            metavar="ID",
            nargs="?",
            type=int)
    note_args.add_argument(
            "-m", "--message",
            help="the content of the note; optional (will"
                 " otherwise be read from standard input)")


def cmd_note(tl, args):
    ts = args.timestamp
    if args.id is None:
        stl = tl.active_timeline
    else:
        stl = tl.get_specific_timeline(args.id, create=False)

    if stl:
        message = args.message
        if message is None:
            try:
                message = input("> ")
            except EOFError:
                message = None
        if message:
            message = message.strip()

        if message:
            try:
                print_entries(stl.note(ts, message))
            except ConsistencyError as e:
                print(e)
        else:
            print("No message.")
    else:
        print("No active timeline." if args.id is None
              else "No such timeline.")
